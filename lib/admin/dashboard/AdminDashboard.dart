import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class AdminDashboard extends StatefulWidget {
  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
                    child: Row(
                      children: [
                        _dashboardButton("document.png",ORDER_HISTORY, (){
                          // navigateRoute(context, "/roomList");
                        }),
                        Container(width: 20,),
                        _dashboardButton("search.png",SEARCH_ROOM, (){
                          navigateRoute(context, "/adminRoomList");
                        }),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
                    child: Row(
                      children: [
                        _dashboardButton("room_type.png",ROOM_TYPE, (){
                          navigateRoute(context, "/adminRoomType");
                        }),
                        Container(width: 20,),
                        _dashboardButton("plus_circle.png",ADD_ROOM, (){
                          navigateRoute(context, "/adminEditRoom");
                        }),
                      ],
                    ),
                  ),
                  Container(
                    child: GridView.count(
                      shrinkWrap: true,
                      primary: true,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 3,
                      children: [
                        _roomItem(1, true),
                        _roomItem(2, true),
                        _roomItem(3, false),
                        _roomItem(4, true),
                        _roomItem(5, false),
                        _roomItem(6, true),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _roomItem(int number, bool free){
    return InkWell(
      onTap: (){
        navigateRoute(context, "/adminRoomDetail");
      },
      child: Container(
        child: Column(
          children: [
            Image.asset("assets/images/${free ? "icon.png": "icon_with_smoke.png"}", width: 60,color: free ? successColor : errorColor,),
            Container(height: 5,),
            Text(
              number.toString(),
              style: TextStyle(
                  fontSize: 18
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _dashboardButton(String asset, String text, Function() onClick){
    return Expanded(
      child: InkWell(
        onTap: () {
          onClick();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset("assets/images/$asset", height: 24, color: Colors.black,),
              Container(height: 5,),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){
    return Container(
      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 40),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(dashboardAppbarRadius),
            bottomLeft: Radius.circular(dashboardAppbarRadius),
          )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset("assets/images/icon.png", width: 50,),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              APP_NAME,
              style: TextStyle(
                  fontSize: 34,
                  color: Colors.white,
                  fontWeight: FontWeight.w900
              ),
            ),
          ),],
      ),
    );
  }
}
