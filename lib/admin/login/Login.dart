import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  FocusNode _userNameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding * 2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        ADMIN,
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                      Container(height: 30,),
                      MyTextField(
                        controller: _userNameController,
                        myFocus: _userNameFocus,
                        nextFocus: _passwordFocus,
                        suffixPath: "person.png",
                        hint: LOGIN_NAME,
                      ),
                      Container(height: 20,),
                      MyTextField(
                        controller: _passwordController,
                        myFocus: _passwordFocus,
                        suffixPath: "eye.png",
                        hint: PASSWORD,
                      ),
                      Container(height: 30,),
                      MyButton(
                        text: LOGIN,
                        onClick: (){
                          navigateRoute(context, "/adminDashboard");
                        },
                        buttonColor: Colors.black,
                        suffix: false,
                      )
                    ],
                  ),
                ),
              ),
            )
          )
        ],
      ),
    );
  }

  Widget _appbar(){
    return Container(
      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 40),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(dashboardAppbarRadius),
            bottomLeft: Radius.circular(dashboardAppbarRadius),
          )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset("assets/images/icon.png", width: 50,),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              APP_NAME,
              style: TextStyle(
                  fontSize: 34,
                  color: Colors.white,
                  fontWeight: FontWeight.w900
              ),
            ),
          ),],
      ),
    );
  }
}

