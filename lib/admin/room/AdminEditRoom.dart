import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';

class AdminEditRoom extends StatefulWidget {
  @override
  _AdminEditRoomState createState() => _AdminEditRoomState();
}

class _AdminEditRoomState extends State<AdminEditRoom> {
  TextEditingController _roomNumberController = TextEditingController();
  TextEditingController _roomPriceController = TextEditingController();

  TextEditingController _roomDetailController = TextEditingController();
  TextEditingController _roomPolicyController = TextEditingController();

  FocusNode _roomNumberFocus = FocusNode();
  FocusNode _roomPriceFocus = FocusNode();

  FocusNode _roomDetailFocus = FocusNode();
  FocusNode _roomPolicyFocus = FocusNode();

  int _showerCount = 1;
  int _bedCount = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(ROOM, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(height: 30,),
              MyTextField(
                hint: ROOM_NUMBER,
                suffixPath: "hash.png",
                controller: _roomNumberController,
                myFocus: _roomNumberFocus,
                nextFocus: _roomPriceFocus,
                textInputType: TextInputType.number,
              ),
              Container(height: 20,),
              MyTextField(
                hint: ROOM_PRICE,
                suffixPath: "dollar_sign.png",
                controller: _roomPriceController,
                myFocus: _roomPriceFocus,
                textInputType: TextInputType.number,
                isFormatted: true,
              ),
              Container(height: 20,),
              _counterItem(SHOWER,_showerCount, (value){
                setState(() {
                  _showerCount = value;
                });
              }),
              Container(height: 20,),
              _counterItem(BED_COUNT,_bedCount, (value){
                setState(() {
                  _bedCount = value;
                });
              }),
              Container(height: 20,),
              Text(
                ROOM_DETAIL,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 16
                ),
              ),
              Container(height: 10,),
              MyTextField(
                height: 120,
                maxLines: 5,
                controller: _roomDetailController,
                myFocus: _roomDetailFocus,
                nextFocus: _roomPolicyFocus,
              ),
              Container(height: 20,),
              Text(
                ROOM_POLICY,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 16
                ),
              ),
              Container(height: 10,),
              MyTextField(
                height: 120,
                maxLines: 5,
                controller: _roomPolicyController,
                myFocus: _roomPolicyFocus,
              ),
              Container(height: 20,),
              MyButton(
                text: SAVE,
                buttonColor: Colors.black,
                suffix: false,
              ),
              Container(height: 20,),
            ],
          ),
        ),
      ),
    );
  }

  Widget _counterItem(String text, int value, Function(int) onchange){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: TextStyle(
                fontSize: 20,
                fontFamily: FONT_RUBIK
            ),
          ),
          InkWell(
              onTap: (){
                if(value > 1){
                  setState(() {
                    value -= 1;
                  });
                  onchange(value);
                }
              },
              child: Icon(Icons.remove_circle, color: Colors.black, size: 30,)
          ),
          Text(
            value.toString(),
            style: TextStyle(
                fontSize: 20,
                fontFamily: FONT_RUBIK
            ),
          ),
          InkWell(
              onTap: (){
                setState(() {
                  value += 1;
                });
                onchange(value);
              },
              child: Icon(Icons.add_circle, color: Colors.black, size: 30,)
          ),
        ],
      ),
    );
  }

}