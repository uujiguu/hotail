import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/room/RoomCardItem.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';


class AdminRoomDetail extends StatefulWidget {
  @override
  _AdminRoomDetailState createState() => _AdminRoomDetailState();
}

class _AdminRoomDetailState extends State<AdminRoomDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(ROOM_DETAIL, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding, vertical: 10),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    RoomCardItem(iconColor: Colors.black,),
                    _infoItem(ROOM_INFO,"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias asperiores atque consectetur consequuntur, delectus dolore dolorum est excepturi facere hic id incidunt inventore minima minus modi molestiae nam natus non odit omnis pariatur quibusdam reprehenderit sint unde ut velit? A dolore ducimus, hic illum ipsam numquam porro quas veritatis?"),
                    _infoItem(ROOM_POLICY,"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias asperiores atque consectetur consequuntur, delectus dolore dolorum est excepturi facere hic id incidunt inventore minima minus modi molestiae nam natus non odit omnis pariatur quibusdam reprehenderit sint unde ut velit? A dolore ducimus, hic illum ipsam numquam porro quas veritatis?"),
                  ],
                ),
              ),
            ),
            MyButton(
              text: EDIT_ROOM,
              buttonColor: Colors.black,
              suffix: false,
              onClick: (){
                navigateRoute(context, "/adminEditRoom");
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _infoItem(String title, String body){
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16
            ),
          ),
          Container(height: 10,),
          Text(
            body,
            style: TextStyle(
                color:commonTextColor
            ),
          )
        ],
      ),
    );
  }
}
