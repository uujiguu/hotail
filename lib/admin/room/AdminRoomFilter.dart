import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyDropDown.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class AdminRoomFilter extends StatefulWidget {
  final Function(int) onBedCountChange;
  final int initBedCount;
  final double minAmount;
  final double maxAmount;

  const AdminRoomFilter({this.onBedCountChange,this.initBedCount = 1, this.minAmount = 0, this.maxAmount = 500000});
  @override
  _AdminRoomFilterState createState() => _AdminRoomFilterState();
}

class _AdminRoomFilterState extends State<AdminRoomFilter> {
  int _value;
  double _minAmount;
  double _maxAmount;
  RangeValues _sliderValue;

  bool _isFree = false;
  bool _isOrdered = false;
  bool _isOccupied = false;

  @override
  void initState() {
    super.initState();
    _value = widget.initBedCount;
    _minAmount = widget.minAmount;
    _maxAmount = widget.maxAmount;
    _sliderValue = RangeValues(_minAmount, _maxAmount);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            FILTER,
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w600
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40, left: 50, right: 50),
            child: Column(
              children: [
                _checkBoxItem(OCCUPIED,(value){setState(() { _isOccupied = value; });}, _isOccupied),
                _checkBoxItem(ORDERED,(value){setState(() { _isOrdered = value; });}, _isOrdered),
                _checkBoxItem(FREE,(value){setState(() { _isFree = value; });}, _isFree),
                _counterItem(),
                Container(height: 20,),
                _sliderItem(),
                Container(height: 20,),
                MyButton(
                  text: TO_FILTER,
                  suffix: false,
                  buttonColor: Colors.black,
                ),
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      CANCEL,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _checkBoxItem(String text, Function(bool) onChanged, bool value){
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Checkbox(
                value: value,
                onChanged: onChanged,
                activeColor: Colors.black,
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w300
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _sliderItem(){
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "$AMOUNT_LIMIT: ",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontFamily: FONT_RUBIK
                ),
              ),
              Text(
                "${formatValueWithComma(_minAmount.toInt())}  -  ${formatValueWithComma(_maxAmount.toInt())}",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontFamily: FONT_MONTSERRAT
                ),
              )
            ],
          ),
          Container(height: 20,),
          Row(
            children: [
              Text(
                "${formatValueWithComma(widget.minAmount.toInt())}",
                style: TextStyle(
                    fontFamily: FONT_MONTSERRAT
                ),
              ),
              Spacer(),
              Text(
                "${formatValueWithComma(widget.maxAmount.toInt())}",
                style: TextStyle(
                    fontFamily: FONT_MONTSERRAT
                ),
              ),
            ],
          ),
          Container(
            child: RangeSlider(
              onChanged: (value){
                _sliderValue = value;
                _minAmount = value.start;
                _maxAmount = value.end;
                setState(() {});
              },
              values: _sliderValue,
              min: widget.minAmount,
              max: widget.maxAmount,
              activeColor: Colors.black,
              inactiveColor: inactiveColor,
              divisions: (widget.maxAmount - widget.minAmount) ~/ 5000,
            ),
          ),
        ],
      ),
    );
  }

  Widget _counterItem(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          BED_COUNT,
          style: TextStyle(
              fontSize: 20,
              fontFamily: FONT_RUBIK
          ),
        ),
        InkWell(
            onTap: (){
              if(_value > 1){
                setState(() {
                  _value -= 1;
                });
                if(widget.onBedCountChange != null){
                  widget.onBedCountChange(_value);
                }
              }
            },
            child: Icon(Icons.remove_circle, color: Colors.black, size: 30,)
        ),
        Text(
          _value.toString(),
          style: TextStyle(
              fontSize: 20,
              fontFamily: FONT_RUBIK
          ),
        ),
        InkWell(
            onTap: (){
              setState(() {
                _value += 1;
              });
              if(widget.onBedCountChange != null){
                widget.onBedCountChange(_value);
              }
            },
            child: Icon(Icons.add_circle, color: Colors.black, size: 30,)
        ),
      ],
    );
  }
}
