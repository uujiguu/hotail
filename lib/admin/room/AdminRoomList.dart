import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotail/custom/BottomSheet.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/RoomFilter.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class AdminRoomList extends StatefulWidget {
  @override
  _AdminRoomListState createState() => _AdminRoomListState();
}

class _AdminRoomListState extends State<AdminRoomList> {
  int _filterRoomType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          ROOM_LIST,
          style: appbarTitleStyle,
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: InkWell(
              onTap: (){
                showAdminRoomFilterBottomSheet(context);
              },
              child: Icon(
                Icons.filter_list,
              ),
            ),
          )
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    _typeRow(),
                    _roomItem(0),
                    _roomItem(1),
                    _roomItem(2),
                    _roomItem(3),
                    _roomItem(4),
                    _roomItem(5),
                    _roomItem(6),
                    _roomItem(7),
                    _roomItem(8),
                    _roomItem(9),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _roomItem(int id){
    return InkWell(
      onTap: (){
        navigateRoute(context, "/adminRoomDetail");
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: baseHorizontalPadding),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white,
            boxShadow:[
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: Offset(0,1),
                blurRadius: 10,
              )
            ]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset("assets/images/temp_room.png", fit: BoxFit.cover,height: 120, width: 120,),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: 6,),
                      Row(
                        children: [
                          Text(
                            "225",
                            style: TextStyle(
                              fontWeight: FontWeight.w900,
                              fontSize: 24,
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Сул",
                            style: TextStyle(
                              color: successColor,
                              fontWeight: FontWeight.w800,
                              fontSize: 18
                            ),
                          ),
                        ],
                      ),
                      Container(height: 6,),
                      Row(
                        children: [
                          Text(
                            "$TYPE:",
                            style: TextStyle(
                              color: commonTextColor,
                              fontSize: 18,
                              fontWeight: FontWeight.w900
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                            margin: EdgeInsets.only(left: 6),
                            decoration: BoxDecoration(
                                color: successColor,
                                borderRadius: BorderRadius.circular(16)
                            ),
                            child: Text(
                              "VIP",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(height: 6,),
                      Row(
                        children: [
                          Text(
                            "$BED_COUNT:",
                            style: TextStyle(
                                color: commonTextColor,
                                fontSize: 16
                            ),
                          ),
                          Container(width: 10,),
                          Text(
                            "4",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 16
                            ),
                          ),
                        ],
                      ),
                      Container(height: 6,), 
                      Row(
                        children: [
                          Text(
                            "26000₮(хоног)",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w900,
                                fontSize: 18
                            ),
                          ),
                          Spacer(),
                          Image.asset("assets/images/edit.png", height: 18, width: 18,)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _typeRow(){
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            _typeItem(0),
            _typeItem(1),
            _typeItem(2),
            _typeItem(3),
          ],
        ),
      ),
    );
  }

  Widget _typeItem(int id){
    return InkWell(
      onTap: (){
        setState(() {
          if(_filterRoomType == id)
            _filterRoomType = null;
          else
            _filterRoomType = id;
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: _filterRoomType == id ? Colors.black : Colors.black.withOpacity(0.3),
                offset: Offset(0,1),
                blurRadius: 10,
              )
            ]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset("assets/images/temp_room.png", width: 126, height: 126, fit: BoxFit.cover,),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4,),
                  margin: EdgeInsets.only(bottom: 10),
                  child: Text(
                    NORMAL_ROOM,
                    style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.w900,
                      fontFamily: FONT_RUBIK,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
