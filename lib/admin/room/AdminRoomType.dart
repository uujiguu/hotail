import 'package:flutter/material.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class AdminRoomType extends StatefulWidget {
  @override
  _AdminRoomTypeState createState() => _AdminRoomTypeState();
}

class _AdminRoomTypeState extends State<AdminRoomType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          ROOM_TYPE,
          style: appbarTitleStyle,
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          children: [
            _typeItem(),
            _typeItem(),
            _typeItem(),
            _typeItem(),
          ],
        ),
      ),
    );
  }

  Widget _typeItem(){
    return InkWell(
      onTap: (){
        navigateRoute(context, "/adminRoomTypeDetail");
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: Offset(0,1),
                blurRadius: 10,
              )
            ]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset("assets/images/temp_room.png", height: 115, fit: BoxFit.cover,),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4,),
                  margin: EdgeInsets.only(bottom: 10),
                  child: Text(
                    NORMAL_ROOM,
                    style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.w900,
                      fontFamily: FONT_RUBIK,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
