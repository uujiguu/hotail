import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';

class AdminRoomTypeDetail extends StatefulWidget {
  @override
  _AdminRoomTypeDetailState createState() => _AdminRoomTypeDetailState();
}

class _AdminRoomTypeDetailState extends State<AdminRoomTypeDetail> {

  TextEditingController _typeNameController = TextEditingController();
  TextEditingController _typeDetailController = TextEditingController();

  FocusNode _typeNameFocus = FocusNode();
  FocusNode _typeDetailFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          ROOM_TYPE,
          style: appbarTitleStyle,
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(height:30),
              MyTextField(
                hint: ROOM_TYPE_NAME,
                suffixPath: 'room_type.png',
                isFormatted: true,
                controller: _typeNameController,
                myFocus: _typeNameFocus,
                nextFocus: _typeDetailFocus,
                textInputType: TextInputType.number,
              ),
              Container(height:40),
              Text(
                ROOM_IMAGE,
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 16
                ),
              ),
              Container(height:20),
              ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Image.asset("assets/images/temp_room.png", fit: BoxFit.cover, width: double.infinity,),
              ),
              Container(height:40),
              Text(
                ROOM_DETAIL,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 16
                ),
              ),
              Container(height:20),
              MyTextField(
                height: 120,
                maxLines: 5,
                controller: _typeDetailController,
                myFocus: _typeDetailFocus,
              ),
              Container(height:10),
              MyButton(
                text: SAVE,
                buttonColor: Colors.black,
                suffix: false,
                onClick: (){
                },
              ),
              Container(height:10),
            ],
          ),
        ),
      ),
    );
  }
}
