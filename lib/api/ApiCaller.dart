import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart';

class APICaller {
  Client client = Client();

  ///POST хүсэлт дуудах функц
  Future<dynamic> sendPostRequest(String url, dynamic body, dynamic header, {bool checkServerConnection = false}) async {
    bool _hasInternet = await _isConnectedToNetwork(url, checkServerConnection: checkServerConnection);
    if (_hasInternet) {
      return client.post(url, body: body, headers: header);
    }
    return null;
  }

  ///Get хүсэлт дуудах функц
  Future<dynamic> sendGetRequest(String cleanUrl, String path, dynamic header, {Map<String, String> params, bool checkServerConnection = false}) async {
    bool _hasInternet = await _isConnectedToNetwork(cleanUrl + path, checkServerConnection: checkServerConnection);
    if (_hasInternet) {
      Map<String, String> queryParameters = {};
      if (params != null) queryParameters.addAll(params);
      final uri = cleanUrl.contains("https") ? Uri.https(cleanUrl, path, queryParameters) : Uri.http(cleanUrl, path, queryParameters);
      return client.get(uri, headers: header);
    }
    return null;
  }

  Future<bool> _isConnectedToNetwork(String url, {bool checkServerConnection = false}) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return checkServerConnection ? _isConnectedToServer(url) : true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return checkServerConnection ? _isConnectedToServer(url) : true;
    }
    return false;
  }

  Future<bool> _isConnectedToServer(String url) async {
    try {
      final result = await InternetAddress.lookup(url);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else
        return false;
    } on SocketException catch (_) {
      return false;
    }
  }
}
