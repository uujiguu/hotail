import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:hotail/api/HotAilRepository.dart';
import 'package:hotail/models/Requests.dart';
import 'package:hotail/models/Responses.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';

final _repository = HotAilRepository();
final roomBloc = RoomBloc();

final _roomTypeFetcher = BehaviorSubject<RoomTypeListResponse>();
Stream<RoomTypeListResponse> get roomTypeData => _roomTypeFetcher.stream;

class RoomBloc {
  dashboardLoaded(dynamic response) {
    _roomTypeFetcher.sink.add(response?.RetType == 0 ? response : null);
  }

  Future<dynamic> roomTypeList() async {
    return _repository.roomTypeList();
  }

  Future<dynamic> roomList() async {
    return _repository.roomList();
  }

}
