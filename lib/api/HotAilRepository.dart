
import 'package:hotail/models/Requests.dart';
import 'package:hotail/models/Responses.dart';

import 'RestAPI.dart';
import 'RestAPI.dart';

class HotAilRepository {
  final apiProvider = RestAPI();

  Future<dynamic> roomTypeList() => apiProvider.roomTypeList();
  Future<dynamic> roomList() => apiProvider.roomList();
}
