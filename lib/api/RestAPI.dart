import 'dart:convert';

import 'package:hotail/api/ApiCaller.dart';
import 'package:hotail/models/Models.dart';
import 'package:hotail/models/Requests.dart';
import 'package:hotail/models/Responses.dart';
import 'package:http/http.dart';

class RestAPI {
  APICaller apiCaller = APICaller();

  static const CLEAN_BASE_URL = "192.168.1.6:8080";

  // static const BASE_URL = "https://$CLEAN_BASE_URL/";
  static const BASE_URL = "http://$CLEAN_BASE_URL/";

  static const QUEST = "quest/";

  static const MY_RESOURCE = "myresource/";
  static const ROOM = "rooms/";

  String _getBaseUrl() => BASE_URL;
  String _getCleanBaseUrl() => CLEAN_BASE_URL;

  Future<dynamic> roomTypeList() async {
    print("here");
    dynamic response = await apiCaller.sendPostRequest(_getBaseUrl() + QUEST + ROOM + "selectType", null, await _getHeader());
    return RoomTypeListResponse.fromJson(_decodeResponse(response));
  }

  Future<dynamic> roomList() async {
    dynamic response = await apiCaller.sendPostRequest(_getBaseUrl() + QUEST + ROOM + "detailType", null, await _getHeader());
    print("type response ${_decodeResponse(response)}");
    return RoomTypeListResponse.fromJson(_decodeResponse(response));
  }


  Future<Map<String, String>> _getHeader() async {
    String token = "no_token";
    // await getToken().then((savedToken) {
    //   if (savedToken != null && savedToken.isNotEmpty) token = savedToken;
    // });

    Map<String, String> header = {
      'Content-type': 'application/json; charset=utf-8',
      'Accept': 'application/json',
    };

    header.addAll({'Authorization': token});
    return header;
  }

  Future<T> _checkResponse<T >(T apiResponse) async {
    return apiResponse;
  }

  dynamic _decodeResponse(Response response) {
    return jsonDecode(response.body);
  }

}