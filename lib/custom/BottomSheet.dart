import 'package:flutter/material.dart';
import 'package:hotail/admin/room/AdminRoomDetail.dart';
import 'package:hotail/admin/room/AdminRoomFilter.dart';
import 'package:hotail/custom/DatePicker.dart';
import 'package:hotail/custom/ExtendBottomSheet.dart';
import 'package:hotail/custom/RoomFilter.dart';

showRoomFilterBottomSheet(BuildContext context){
  showCustomBottomSheet(context, RoomFilter());
}

showAdminRoomFilterBottomSheet(BuildContext context){
  showCustomBottomSheet(context, AdminRoomFilter());
}

showExtendBottomSheet(BuildContext context){
  showCustomBottomSheet(context, ExtendBottomSheet());
}

showCustomBottomSheet(BuildContext context, Widget child, {Color bgColor = Colors.white}) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          width: double.infinity,
          color: Colors.transparent,
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              color: bgColor != null ? bgColor : Colors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
            ),
            child: Wrap(
              children: <Widget>[child],
            ),
          ),
        );
      });
}
