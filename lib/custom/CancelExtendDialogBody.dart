import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class CancelExtend extends StatefulWidget {
  @override
  _CancelExtendState createState() => _CancelExtendState();
}

class _CancelExtendState extends State<CancelExtend> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: buttonHeight / 2, left: 50,right: 50),
            padding: EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14)
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(height: 20,),
                Icon(Icons.warning, size: 36, color: Colors.red,),
                Container(height: 20,),
                Text(
                  CANCEL_EXTEND_TITLE,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 20
                  ),
                ),
                Container(height: 16,),
                Text(
                  CANCEL_EXTEND_BODY,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16
                  ),
                ),
                Container(height: 40,),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  text: CANCEL,
                  suffix: false,
                  isStroked: true,
                  width: 130,
                  onClick: (){
                    Navigator.pop(context);
                  },
                ),
                Container(width: 20, height: 0,),
                MyButton(
                  text: YES,
                  suffix: false,
                  width: 130,
                  onClick: (){
                    navigateRoute(context, "/dashboard", remove: true);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
