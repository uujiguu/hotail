import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';

import 'CancelExtendDialogBody.dart';
import 'SuccessDialogBody.dart';


Future<dynamic> showSuccessDialog(BuildContext context) {
  return showCustomDialog(context: context, customBody: SuccessDialogBody());
}

Future<dynamic> showCancelExtendDialog(BuildContext context) {
  return showCustomDialog(context: context, customBody: CancelExtend());
}

Future<void> showCustomDialog({
  @required BuildContext context,
  bool isDismissible = true,
  Widget customBody,
}) async {
  return await showGeneralDialog<dynamic>(
    context: context,
    barrierDismissible: isDismissible ?? true,
    barrierLabel: '',
    transitionDuration: Duration(milliseconds: 150),
    barrierColor: primaryColor.withOpacity(0.4),
    transitionBuilder: (context, anim1, anim2, child) {
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: anim1.value * 2.5, sigmaY: anim1.value * 2.5),
        child: Transform.scale(
          scale: anim1.value,
          child: AlertDialog(
              insetPadding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: customBody
          ),
        ),
      );
    },
    pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
      return null;
    },
  );
}
