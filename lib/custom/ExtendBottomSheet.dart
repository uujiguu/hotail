import 'package:flutter/material.dart';
import 'package:hotail/custom/Dialog.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import 'MyButton.dart';
import 'MyTextField.dart';

class ExtendBottomSheet extends StatefulWidget {
  @override
  _ExtendBottomSheetState createState() => _ExtendBottomSheetState();
}

class _ExtendBottomSheetState extends State<ExtendBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            EXTEND,
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w600
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40, left: 50, right: 50),
            child: Column(
              children: [
                InkWell(
                  onTap: () async {
                    final List<DateTime> picked = await DateRagePicker.showDatePicker(
                      context: context,
                      initialFirstDate: new DateTime.now(),
                      initialLastDate: (new DateTime.now()).add(new Duration(days: 7)),
                      firstDate: new DateTime(2015),
                      lastDate: new DateTime(2025),
                    );
                    if (picked != null && picked.length == 2) {
                      print(picked);
                    }
                  },
                  child: MyTextField(
                    hint: DATE_HINT,
                    isEditable: false,
                    suffixPath: "calendar.png",
                  ),
                ),
                Container(height: 20,),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "$PAY_AMOUNT:",
                        style: TextStyle(
                          fontFamily: FONT_RUBIK,
                          color: commonTextColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Text(
                      formatValueWithComma(96000),
                      style: TextStyle(
                        fontFamily: FONT_RUBIK,
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ],
                ),
                Container(height: 20,),
                MyTextField(
                  hint: INSERT_PAYMENT,
                  suffixPath: "dollar_sign.png",
                  textInputType: TextInputType.number,
                ),
                Container(height: 40,),
                MyButton(
                  text: EXTEND,
                  suffix: false,
                  onClick: (){
                    showSuccessDialog(context);
                  },
                ),
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      CANCEL,
                      style: TextStyle(
                          color: primaryColor,
                          fontSize: 16
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
