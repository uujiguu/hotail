import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';

class MyButton extends StatefulWidget {
  final String text;
  final Function() onClick;
  final bool suffix;
  final Color buttonColor;
  final Color textColor;
  final bool showShadow;
  final double width;
  final bool isStroked;

  const MyButton({this.text, this.onClick, this.suffix = true, this.buttonColor = primaryColor, this.textColor = Colors.white, this.showShadow, this.width, this.isStroked = false});
  @override
  _MyButtonState createState() => _MyButtonState();
}

class _MyButtonState extends State<MyButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      child: MaterialButton(
        color: widget.isStroked ? Colors.white : widget.buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(buttonRadius),
          side: BorderSide(color: widget.buttonColor),
        ),
        height: buttonHeight,
        onPressed: (){
          if(widget.onClick != null) widget.onClick();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.text??"",
              style: TextStyle(
                color: widget.isStroked ? primaryColor : widget.textColor,
                fontSize: 16
              ),
            ),
            widget.suffix ? Container(
              padding: EdgeInsets.only(left: 6),
              child: Image.asset("assets/images/arrow_right.png"),
            ) : Container(width: 0, height: 0,),
          ],
        ),
      ),
    );
  }
}
