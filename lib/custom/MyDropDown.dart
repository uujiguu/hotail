import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';

class MyDropDown extends StatefulWidget {
  final String label;
  final String hint;
  final String disabledHint;
  final bool editable;
  final String value;
  final FocusNode myFocus;
  final FocusNode nextFocus;
  final List<String> items;
  final Function(String) onChanged;
  final double underlineWidth;
   MyDropDown({this.label, this.items, this.onChanged, this.hint = "", this.value, this.myFocus, this.nextFocus, this.disabledHint, this.editable = true, this.underlineWidth = 2,});

  @override
  _MyDropDownState createState() => _MyDropDownState();
}

class _MyDropDownState extends State<MyDropDown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: textFieldHeight,
      decoration: BoxDecoration(
        border: Border.all(color: commonTextColor ),
        borderRadius: BorderRadius.circular(8),
      ),
      margin: EdgeInsets.symmetric(vertical: 8,),
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.label != null ? Text(
            widget.label,
            style: TextStyle(
              color: commonTextColor,
              fontSize: 12,
              fontFamily: FONT_RUBIK,
            ),
          ) : Container(),
          DropdownButton<String>(
            isDense: true,
            autofocus: true,
            focusNode: widget.myFocus,
            underline: Container(),
            onChanged: widget.editable
                ? (value) {
              if (widget.onChanged != null) widget.onChanged(value);
              if (widget.nextFocus != null) {
                FocusScope.of(context).requestFocus(widget.nextFocus);
              }
            }
                : null,
            iconDisabledColor: Colors.transparent,
            hint: Container(
              margin: EdgeInsets.only(left: 8),
              child: Text(
                widget.hint,
                style: TextStyle(
                  fontFamily: FONT_RUBIK,
                  color: commonTextColor,
                  fontSize: 18,
                ),
              ),
            ),
            disabledHint: Container(
              margin: EdgeInsets.only(left: 8),
              child: Text(
                widget.disabledHint ?? (widget.value ?? CHOOSE),
                style: TextStyle(
                  fontFamily: FONT_RUBIK,
                  color: commonTextColor,
                  fontSize: 18,
                ),
              ),
            ),
            value: widget.value,
            isExpanded: true,
            style: TextStyle(
              fontFamily: FONT_RUBIK,
              color: Colors.black,
              fontSize: 18,
            ),
            items: widget.items != null ? _getItems() : null,
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem<String>> _getItems() {
    List<DropdownMenuItem<String>> _list = [];
    for (int i = 0; i < widget.items.length; i++) {
      _list.add(
        DropdownMenuItem<String>(
          child: Container(
            child: Text(widget.items[i]),
          ),
          value: widget.items[i],
        ),
      );
    }
    return _list;
  }
}
