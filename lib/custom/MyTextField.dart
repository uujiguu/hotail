

import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';

class MyTextField extends StatefulWidget {
  final String label;
  final String hint;
  final String error;
  final FocusNode myFocus;
  final FocusNode nextFocus;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final int maxLen;
  final bool obscure;
  final Function(String) onTextChanged;
  final TextCapitalization textCapitalization;
  final bool isFormatted;
  final bool isEditable;
  final String suffixPath;
  final TextEditingController controller;
  final double height;
  final int maxLines;
  MyTextField({
    this.label,
    this.hint,
    this.onTextChanged,
    this.error,
    this.myFocus,
    this.nextFocus,
    this.textInputAction,
    this.textInputType,
    this.maxLen,
    this.obscure,
    this.textCapitalization,
    this.isEditable,
    this.isFormatted = false,
    this.suffixPath,
    this.height = textFieldHeight,
    this.controller,
    this.maxLines = 1,
  });

  @override
  _MyTextFieldState createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  TextEditingController _textController = TextEditingController();
  final _moneyController = MoneyMaskedTextController(precision: 0, decimalSeparator: "", thousandSeparator: ",");

  @override
  void initState() {
    super.initState();
    if (widget.controller != null && widget.controller.text != null) {
      if (widget.isFormatted)
        _moneyController.text = widget.controller.text;
      else
        _textController.text = widget.controller.text;
    }

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.controller != null && widget.controller.text != null && widget.onTextChanged != null) widget.onTextChanged(widget.controller.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18),
      height: widget.height,
      decoration: BoxDecoration(
        border: Border.all(color: commonTextColor ),
        borderRadius: BorderRadius.circular(8),
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          widget.label != null
              ? Text(
            widget.label ?? "",
            style: TextStyle(
              color: commonTextColor,
              fontSize: 12,
              fontFamily: FONT_RUBIK,
            ),
          )
              : Container(),
          Row(
            children: [
              Expanded(
                child: TextField(
                  maxLines: widget.maxLines,
                  enabled: widget.isEditable ?? true,
                  controller: widget.isFormatted ? _moneyController : _textController,
                  keyboardType: widget.textInputType ?? TextInputType.text,
                  obscureText: widget.obscure ?? false,
                  focusNode: widget.myFocus,
                  maxLength: widget.maxLen,
                  textInputAction: widget.textInputAction ?? TextInputAction.done,
                  textCapitalization: widget.textCapitalization ?? TextCapitalization.sentences,
                  style: TextStyle(
                    fontFamily: FONT_RUBIK,
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    letterSpacing: -0.384,
                  ),
                  showCursor: widget.isFormatted ? false : true,
                  decoration: InputDecoration(
                    counterText: "",
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 0),
                    hintText: widget.hint ?? widget.label ?? "",
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                      fontFamily: FONT_RUBIK,
                      color: commonTextColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      letterSpacing: -0.384,
                    ),
                    errorText: widget.error,

                  ),
                  onChanged: (text) {
                    if (widget.onTextChanged != null) {
                      if (widget.isFormatted)
                        widget.onTextChanged("${_moneyController.numberValue.toInt()}");
                      else
                        widget.onTextChanged(_textController.text);
                    }
                  },
                  onEditingComplete: () => FocusScope.of(context).requestFocus(widget.nextFocus != null ? widget.nextFocus : FocusNode()),
                ),
              ),
              widget.suffixPath != null ?
              Image.asset("assets/images/${widget.suffixPath}", height: 28) :
                  Container(),
            ],
          ),
        ],
      ),
    );
  }
}
