import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyDropDown.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class RoomFilter extends StatefulWidget {
  final Function(int) onBedCountChange;
  final int initBedCount;
  final double minAmount;
  final double maxAmount;

  const RoomFilter({this.onBedCountChange,this.initBedCount = 1, this.minAmount = 0, this.maxAmount = 500000});
  @override
  _RoomFilterState createState() => _RoomFilterState();
}

class _RoomFilterState extends State<RoomFilter> {
  int _value;
  double _minAmount;
  double _maxAmount;
  RangeValues _sliderValue;

  @override
  void initState() {
    super.initState();
    _value = widget.initBedCount;
    _minAmount = widget.minAmount;
    _maxAmount = widget.maxAmount;
    _sliderValue = RangeValues(_minAmount, _maxAmount);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            FILTER,
            style: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w600
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40, left: 50, right: 50),
            child: Column(
              children: [
                _counterItem(),
                Container(height: 20,),
                _sliderItem(),
                Container(height: 20,),
                MyButton(
                  text: TO_FILTER,
                  suffix: false,
                ),
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      CANCEL,
                      style: TextStyle(
                        color: primaryColor,
                        fontSize: 16
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _sliderItem(){
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "$AMOUNT_LIMIT: ",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: FONT_RUBIK
                ),
              ),
              Text(
                "${formatValueWithComma(_minAmount.toInt())}  -  ${formatValueWithComma(_maxAmount.toInt())}",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: FONT_MONTSERRAT
                ),
              )
            ],
          ),
          Container(height: 20,),
          Row(
            children: [
              Text(
                "${formatValueWithComma(widget.minAmount.toInt())}",
                style: TextStyle(
                  fontFamily: FONT_MONTSERRAT
                ),
              ),
              Spacer(),
              Text(
                "${formatValueWithComma(widget.maxAmount.toInt())}",
                style: TextStyle(
                    fontFamily: FONT_MONTSERRAT
                ),
              ),
            ],
          ),
          Container(
            child: RangeSlider(
              onChanged: (value){
                _sliderValue = value;
                _minAmount = value.start;
                _maxAmount = value.end;
                setState(() {});
              },
              values: _sliderValue,
              min: widget.minAmount,
              max: widget.maxAmount,
              activeColor: primaryColor,
              inactiveColor: inactiveColor,
              divisions: (widget.maxAmount - widget.minAmount) ~/ 5000,
            ),
          ),
        ],
      ),
    );
  }

  Widget _counterItem(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          BED_COUNT,
          style: TextStyle(
            fontSize: 20,
            fontFamily: FONT_RUBIK
          ),
        ),
        InkWell(
          onTap: (){
            if(_value > 1){
              setState(() {
                _value -= 1;
              });
              if(widget.onBedCountChange != null){
                widget.onBedCountChange(_value);
              }
            }
          },
          child: Icon(Icons.remove_circle, color: primaryColor, size: 30,)
        ),
        Text(
          _value.toString(),
          style: TextStyle(
              fontSize: 20,
              fontFamily: FONT_RUBIK
          ),
        ),
        InkWell(
            onTap: (){
              setState(() {
                _value += 1;
              });
              if(widget.onBedCountChange != null){
                widget.onBedCountChange(_value);
              }
            },
            child: Icon(Icons.add_circle, color: primaryColor, size: 30,)
        ),
      ],
    );
  }
}
