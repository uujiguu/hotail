import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class SuccessDialogBody extends StatefulWidget {
  @override
  _SuccessDialogBodyState createState() => _SuccessDialogBodyState();
}

class _SuccessDialogBodyState extends State<SuccessDialogBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: buttonHeight / 2),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14)
            ),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(child: Image.asset("assets/images/dialog_background.png")),
                Text(
                  ORDER_SUCCESS,
                  style: TextStyle(
                      fontSize: 18
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  text: OK,
                  suffix: false,
                  width: 150,
                  onClick: (){
                    navigateRoute(context, "/dashboard", remove: true);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
