import 'package:flutter/material.dart';
import 'package:hotail/api/HotAilBloc.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  PageController _controller = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _sliderItem(),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
                    child: Row(
                      children: [
                        _dashboardButton("shower.png",BOOK, (){
                          // navigateRoute(context, "/roomList");
                          print("fuck yes");
                          RoomBloc().roomTypeList().then((response){
                            print("fuck off");
                          });
                        }),
                        Container(width: 20,),
                        _dashboardButton("shower.png",ORDER_SEARCH, (){
                          navigateRoute(context, "/orderDetail");
                        }),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(width: baseHorizontalPadding,),
                      Text(ROOM_LIST, style: TextStyle( fontSize: 20),),
                    ],
                  ),
                  Container(
                    child: GridView.count(
                      shrinkWrap: true,
                      primary: true,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 3,
                      children: [
                        _roomItem(1, true),
                        _roomItem(2, true),
                        _roomItem(3, false),
                        _roomItem(4, true),
                        _roomItem(5, false),
                        _roomItem(6, true),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _roomItem(int number, bool free){
    return InkWell(
      onTap: (){
        navigateRoute(context, "/roomDetail");
      },
      child: Container(
        child: Column(
          children: [
            Image.asset("assets/images/${free ? "icon.png": "icon_with_smoke.png"}", width: 60,color: free ? successColor : errorColor,),
            Container(height: 5,),
            Text(
              number.toString(),
              style: TextStyle(
                fontSize: 18
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _sliderItem(){
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          height: 340,
          child: PageView(
            controller: _controller,
            children: _getPages(),
          ),
        ),
        SmoothPageIndicator(
          effect: ExpandingDotsEffect(
            activeDotColor: primaryColor,
            dotColor: inactiveColor,
            dotHeight: 10,
            dotWidth: 10,
          ),
          controller: _controller,
          count: 5,
        ),
      ],
    );
  }

  Widget _dashboardButton(String asset, String text, Function() onClick){
    return Expanded(
      child: InkWell(
        onTap: () {
          onClick();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: primaryColor, width: 2),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset("assets/images/$asset", height: 24,),
              Container(height: 5,),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _getPages(){
    List<Widget> widgets = [];
    for(int i=0; i < 5 ; i++){
      widgets.add(_cardItem());
    }
    return widgets;
  }

  Widget _appbar(){
    return Container(
      padding: EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 40),
      width: double.infinity,
      decoration: BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(dashboardAppbarRadius),
            bottomLeft: Radius.circular(dashboardAppbarRadius),
          )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: (){
              navigateRoute(context, "/login");
            },
            child: Image.asset("assets/images/icon.png", width: 50,),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              APP_NAME,
              style: TextStyle(
                  fontSize: 34,
                  color: Colors.white,
                  fontWeight: FontWeight.w900
              ),
            ),
          ),],
      ),
    );
  }

  Widget _cardItem(){
    return InkWell(
      onTap: (){
        navigateRoute(context, "/roomList");
      },
      child: Container(
        margin: EdgeInsets.only(left: baseHorizontalPadding, right: baseHorizontalPadding, bottom: 20, top: 10),
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(
              color: Colors.black.withOpacity(0.3),
              offset: Offset(0,1),
              blurRadius: 10,
            )]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(dialogRadius),
          child: Container(
            padding: EdgeInsets.only(bottom: 12),
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset("assets/images/temp_room.png", width: double.infinity, fit: BoxFit.cover,),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        child: Text(
                          NORMAL_ROOM,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w900,
                            fontFamily: FONT_RUBIK,
                          ),
                        ),
                      ),
                      // Row(
                      //   children: [
                      //     Spacer(),
                      //     Image.asset("assets/images/shower.png", width: 20,),
                      //     Container(width: 8,),
                      //     Text(
                      //       "2 $SHOWER",
                      //       style: TextStyle(
                      //           fontFamily: FONT_RUBIK,
                      //           fontSize: 13,
                      //           fontWeight: FontWeight.w600
                      //       ),
                      //     ),
                      //     Container(width: 12,),
                      //     Image.asset("assets/images/bed.png", width: 20,),
                      //     Container(width: 8,),
                      //     Text(
                      //       "2 $BED",
                      //       style: TextStyle(
                      //           fontFamily: FONT_RUBIK,
                      //           fontSize: 13,
                      //           fontWeight: FontWeight.w600
                      //       ),
                      //     ),
                      //   ],
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
