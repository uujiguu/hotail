import 'package:flutter/material.dart';
import 'package:hotail/admin/dashboard/AdminDashboard.dart';
import 'package:hotail/admin/login/Login.dart';
import 'package:hotail/admin/room/AdminEditRoom.dart';
import 'package:hotail/admin/room/AdminRoomDetail.dart';
import 'package:hotail/admin/room/AdminRoomList.dart';
import 'package:hotail/admin/room/AdminRoomType.dart';
import 'package:hotail/admin/room/AdminRoomTypeDetail.dart';
import 'package:hotail/dashboard/Dashboard.dart';
import 'package:hotail/order/OrderDetail.dart';
import 'package:hotail/order/OrderRoom.dart';
import 'package:hotail/room/RoomDetail.dart';
import 'package:hotail/room/RoomList.dart';
import 'package:hotail/room/Terms.dart';
import 'package:hotail/splash/Splash.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Stetho.initialize();
  return runApp(MyApp());
}

//export PATH="$PATH:/Users/uujiguu/Desktop/data/flutter/bin"

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: baseTheme,
      initialRoute: "/",
      routes: <String, WidgetBuilder>{
        "/": (context) => Splash(),
        "/splash": (context) => Splash(),
        "/dashboard": (context) => Dashboard(),
        "/roomDetail": (context) => RoomDetail(),
        "/roomList": (context) => RoomList(),
        "/orderDetail": (context) => OrderDetail(),
        "/orderRoom": (context) => OrderRoom(),
        "/terms": (context) => Terms(),
        "/login": (context) => Login(),
        "/adminDashboard": (context) => AdminDashboard(),
        "/adminRoomList": (context) => AdminRoomList(),
        "/adminRoomDetail": (context) => AdminRoomDetail(),
        "/adminEditRoom": (context) => AdminEditRoom(),
        "/adminRoomType": (context) => AdminRoomType(),
        "/adminRoomTypeDetail": (context) => AdminRoomTypeDetail(),
      },
    );
  }
}
