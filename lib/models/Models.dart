import 'dart:ui';

import 'package:json_annotation/json_annotation.dart';
part 'Models.g.dart';

//flutter packages pub run build_runner build

@JsonSerializable()
class RoomTypeModel{
  String image;
  String name;
  String typeId;

  RoomTypeModel();
  factory RoomTypeModel.fromJson(Map<String, dynamic> json) => _$RoomTypeModelFromJson(json);
  Map<String, dynamic> toJson() => _$RoomTypeModelToJson(this);
}