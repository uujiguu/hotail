// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomTypeModel _$RoomTypeModelFromJson(Map<String, dynamic> json) {
  return RoomTypeModel()
    ..image = json['image'] as String
    ..name = json['name'] as String
    ..typeId = json['typeId'] as String;
}

Map<String, dynamic> _$RoomTypeModelToJson(RoomTypeModel instance) =>
    <String, dynamic>{
      'image': instance.image,
      'name': instance.name,
      'typeId': instance.typeId,
    };
