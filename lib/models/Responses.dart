import 'package:json_annotation/json_annotation.dart';

import 'Models.dart';
part 'Responses.g.dart';

//flutter packages pub run build_runner build

//export PATH="$PATH:/Users/almabyeknasipad/Desktop/work/flutter/bin"

@JsonSerializable()
class RoomTypeListResponse {
  List<RoomTypeModel> roomTypes;

  RoomTypeListResponse();
  factory RoomTypeListResponse.fromJson(Map<String, dynamic> json) => _$RoomTypeListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$RoomTypeListResponseToJson(this);
}
