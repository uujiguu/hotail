// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomTypeListResponse _$RoomTypeListResponseFromJson(Map<String, dynamic> json) {
  return RoomTypeListResponse()
    ..roomTypes = (json['roomTypes'] as List)
        ?.map((e) => e == null
            ? null
            : RoomTypeModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomTypeListResponseToJson(
        RoomTypeListResponse instance) =>
    <String, dynamic>{
      'roomTypes': instance.roomTypes,
    };
