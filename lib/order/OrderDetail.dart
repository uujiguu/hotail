import 'package:flutter/material.dart';
import 'package:hotail/custom/BottomSheet.dart';
import 'package:hotail/custom/Dialog.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/room/RoomCardItem.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class OrderDetail extends StatefulWidget {
  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  bool _loaded = false;
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ORDER_DETAIL, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(height: 12,),
              Row(
                children: [
                  Expanded(
                    child: MyTextField(
                      hint: ORDER_NUMBER,
                      controller: _controller,
                      textInputType: TextInputType.number,
                    ),
                  ),
                  Container(width: 20,),
                  _circleButton(),
                ],
              ),
              Container(height: 20,),
              _loaded ? Container(
                child: Column(
                  children: [
                    RoomCardItem(),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "$ORDER_NUMBER:",
                                style: TextStyle(
                                  fontSize: 22
                                ),
                              ),
                              Spacer(),
                              Text(
                                "154513",
                                style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w900
                                ),
                              ),
                            ],
                          ),
                          Container(height: 16,),
                          Row(
                            children: [
                              Text(
                                START_DATE,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: commonTextColor
                                ),
                              ),
                              Spacer(),
                              Text(
                                END_DATE,
                                style: TextStyle(
                                  fontSize: 14,
                                    color: commonTextColor
                                ),
                              ),
                            ],
                          ),
                          Container(height: 10,),
                          Row(
                            children: [
                              Text(
                                "01/01/2020",
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                              Spacer(),
                              Text(
                                "01/01/2020",
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                          Container(height: 16,),
                          Text(
                            PHONE_NUMBER,
                            style: TextStyle(
                                fontSize: 14,
                                color: commonTextColor
                            ),
                          ),
                          Container(height: 10,),
                          Text(
                            "99690752",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          Container(height: 16,),
                          Text(
                            PAYMENT_AMOUNT,
                            style: TextStyle(
                                fontSize: 14,
                                color: commonTextColor
                            ),
                          ),
                          Container(height: 10,),
                          Text(
                            formatValueWithComma(50000),
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          Container(height: 16,),
                          MyButton(
                            text: EXTEND,
                            suffix: false,
                            onClick: (){
                              showExtendBottomSheet(context);
                            },
                          ),
                          InkWell(
                            onTap: (){
                              showCancelExtendDialog(context);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                CANCEL,
                                style: TextStyle(
                                    color: primaryColor,
                                    fontSize: 16
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ) : Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget _circleButton(){
    return InkWell(
      onTap: (){
        _loaded = true;
        setState(() { });
      },
      child: Container(
        height: buttonHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: primaryColor
        ),
        child: Container(child: Image.asset("assets/images/arrow_right.png")),
      ),
    );
  }
}
