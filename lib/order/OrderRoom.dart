import 'package:flutter/material.dart';
import 'package:hotail/custom/BottomSheet.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/custom/MyTextField.dart';
import 'package:hotail/main.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class OrderRoom extends StatefulWidget {
  @override
  _OrderRoomState createState() => _OrderRoomState();
}

class _OrderRoomState extends State<OrderRoom> {
  FocusNode _lastNameFocus = FocusNode();
  FocusNode _firstNameFocus = FocusNode();
  FocusNode _phoneNumberFocus = FocusNode();
  FocusNode _registerNumberFocus = FocusNode();
  FocusNode _paymentFocus = FocusNode();

  TextEditingController _dateController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _registerNumberController = TextEditingController();
  TextEditingController _paymentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ORDER_ROOM, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      child: Row(
                        children: [
                          Text(
                            ROOM_NUMBER+" ",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            "225",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w900
                            ),
                          )
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Image.asset("assets/images/shower.png", width: 20,),
                        Container(width: 8,),
                        Text(
                          "2 $SHOWER",
                          style: TextStyle(
                              fontFamily: FONT_RUBIK,
                              fontSize: 13,
                              fontWeight: FontWeight.w600
                          ),
                        ),
                        Container(width: 12,),
                        Image.asset("assets/images/bed.png", width: 20,),
                        Container(width: 8,),
                        Text(
                          "2 $BED",
                          style: TextStyle(
                              fontFamily: FONT_RUBIK,
                              fontSize: 13,
                              fontWeight: FontWeight.w600
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
                      child: Column(
                        children: [
                          InkWell(
                            onTap: () async {
                              final List<DateTime> picked = await DateRagePicker.showDatePicker(
                                context: context,
                                initialFirstDate: new DateTime.now(),
                                initialLastDate: (new DateTime.now()).add(new Duration(days: 7)),
                                firstDate: new DateTime(2015),
                                lastDate: new DateTime(2025),
                              );
                              if (picked != null && picked.length == 2) {
                                print(picked);
                              }
                            },
                            child: MyTextField(
                              hint: DATE_HINT,
                              isEditable: false,
                              suffixPath: "calendar.png",
                              controller: _dateController,
                            ),
                          ),
                          Container(height: 20,),
                          MyTextField(
                            hint: ORDER_LAST_NAME,
                            suffixPath: "person.png",
                            controller: _lastNameController,
                            myFocus: _lastNameFocus,
                            nextFocus: _firstNameFocus,
                          ),
                          Container(height: 20,),
                          MyTextField(
                            hint: ORDER_FIRST_NAME,
                            suffixPath: "person.png",
                            controller: _firstNameController,
                            myFocus: _firstNameFocus,
                            nextFocus: _phoneNumberFocus,
                          ),
                          Container(height: 20,),
                          MyTextField(
                            hint: PHONE_NUMBER,
                            suffixPath: "phone.png",
                            controller: _phoneNumberController,
                            myFocus: _phoneNumberFocus,
                            nextFocus: _registerNumberFocus,
                            textInputType: TextInputType.number,
                            maxLen: 8,
                          ),
                          Container(height: 20,),
                          MyTextField(
                            hint: REGISTER_NUMBER,
                            suffixPath: "hash.png",
                            controller: _registerNumberController,
                            myFocus: _registerNumberFocus,
                            nextFocus: _paymentFocus,
                          ),
                          Container(height: 20,),
                          MyTextField(
                            hint: INSERT_PAYMENT,
                            suffixPath: "dollar_sign.png",
                            controller: _paymentController,
                            myFocus: _paymentFocus,
                            textInputType: TextInputType.number,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Text(
                    TOTAL_PAYMENT,
                    style: TextStyle(
                      fontFamily: FONT_RUBIK,
                      color: commonTextColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Text(
                  formatValueWithComma(96000),
                  style: TextStyle(
                    fontFamily: FONT_RUBIK,
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: MyButton(
                text: CONTINUE,
                suffix: false,
                onClick: (){
                  navigateRoute(context, "/terms");
                },
              ),
            ),
          ],
        ),
      )
    );
  }
}
