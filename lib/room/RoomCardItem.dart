import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';

class RoomCardItem extends StatefulWidget {
  final Color iconColor;

  const RoomCardItem({this.iconColor = primaryColor});
  @override
  _RoomCardItemState createState() => _RoomCardItemState();
}

class _RoomCardItemState extends State<RoomCardItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Text(
                ROOM_NUMBER+" ",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              Text(
                "225",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w900
                ),
              )
            ],
          ),
          _imageItem(),
        ],
      ),
    );
  }

  Widget _imageItem(){
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          Stack(
            alignment: Alignment.bottomRight,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 14),
                child: ClipRRect(borderRadius: BorderRadius.circular(16), child: Image.asset("assets/images/temp_room.png", fit: BoxFit.cover, width: double.infinity, height: 150,)),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 12,vertical: 6),
                    margin: EdgeInsets.only(right: 14),
                    decoration: BoxDecoration(
                        color: widget.iconColor,
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: Text(
                      "VIP",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          Row(
            children: [
              Image.asset("assets/images/shower.png", width: 20,color: widget.iconColor),
              Container(width: 8,),
              Text(
                "2 $SHOWER",
                style: TextStyle(
                    fontFamily: FONT_RUBIK,
                    fontSize: 13,
                    fontWeight: FontWeight.w600
                ),
              ),
              Container(width: 12,),
              Image.asset("assets/images/bed.png", width: 20, color: widget.iconColor,),
              Container(width: 8,),
              Text(
                "2 $BED",
                style: TextStyle(
                    fontFamily: FONT_RUBIK,
                    fontSize: 13,
                    fontWeight: FontWeight.w600
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

