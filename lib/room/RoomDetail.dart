import 'package:flutter/material.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Consts.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

import 'RoomCardItem.dart';

class RoomDetail extends StatefulWidget {
  @override
  _RoomDetailState createState() => _RoomDetailState();
}

class _RoomDetailState extends State<RoomDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ROOM_DETAIL, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding, vertical: 10),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    RoomCardItem(),
                    _infoItem(ROOM_INFO,"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias asperiores atque consectetur consequuntur, delectus dolore dolorum est excepturi facere hic id incidunt inventore minima minus modi molestiae nam natus non odit omnis pariatur quibusdam reprehenderit sint unde ut velit? A dolore ducimus, hic illum ipsam numquam porro quas veritatis?"),
                    _infoItem(ROOM_POLICY,"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias asperiores atque consectetur consequuntur, delectus dolore dolorum est excepturi facere hic id incidunt inventore minima minus modi molestiae nam natus non odit omnis pariatur quibusdam reprehenderit sint unde ut velit? A dolore ducimus, hic illum ipsam numquam porro quas veritatis?"),
                  ],
                ),
              ),
            ),
            MyButton(
              text: BOOK_ROOM,
              onClick: (){
                navigateRoute(context, "/orderRoom");
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _infoItem(String title, String body){
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16
            ),
          ),
          Container(height: 10,),
          Text(
            body,
            style: TextStyle(
              color:commonTextColor
            ),
          )
        ],
      ),
    );
  }
}
