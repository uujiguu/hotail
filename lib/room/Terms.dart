import 'package:flutter/material.dart';
import 'package:hotail/custom/Dialog.dart';
import 'package:hotail/custom/MyButton.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';

class Terms extends StatefulWidget {
  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TERMS, style: appbarTitleStyle,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: baseHorizontalPadding),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "1. Title",
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto consectetur, corporis dignissimos dolor doloremque earum enim esse et harum id impedit incidunt laborum mollitia nam necessitatibus neque, nisi odio odit optio, quo ratione sunt. Facere fuga fugit magnam nihil officia reiciendis, tempore! Atque autem, excepturi exercitationem ipsum molestiae similique!",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "1. Title",
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto consectetur, corporis dignissimos dolor doloremque earum enim esse et harum id impedit incidunt laborum mollitia nam necessitatibus neque, nisi odio odit optio, quo ratione sunt. Facere fuga fugit magnam nihil officia reiciendis, tempore! Atque autem, excepturi exercitationem ipsum molestiae similique!",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "1. Title",
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto consectetur, corporis dignissimos dolor doloremque earum enim esse et harum id impedit incidunt laborum mollitia nam necessitatibus neque, nisi odio odit optio, quo ratione sunt. Facere fuga fugit magnam nihil officia reiciendis, tempore! Atque autem, excepturi exercitationem ipsum molestiae similique!",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "1. Title",
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto consectetur, corporis dignissimos dolor doloremque earum enim esse et harum id impedit incidunt laborum mollitia nam necessitatibus neque, nisi odio odit optio, quo ratione sunt. Facere fuga fugit magnam nihil officia reiciendis, tempore! Atque autem, excepturi exercitationem ipsum molestiae similique!",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "1. Title",
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto consectetur, corporis dignissimos dolor doloremque earum enim esse et harum id impedit incidunt laborum mollitia nam necessitatibus neque, nisi odio odit optio, quo ratione sunt. Facere fuga fugit magnam nihil officia reiciendis, tempore! Atque autem, excepturi exercitationem ipsum molestiae similique!",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                  ],
                ),
              ),
            ),
            MyButton(
              text: AGREE,
              suffix: false,
              onClick: (){
                showSuccessDialog(context);
              },
            ),
            InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  CANCEL,
                  style: TextStyle(
                      color: primaryColor,
                      fontSize: 16
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
