import 'package:flutter/material.dart';
import 'package:hotail/utils/BaseStyle.dart';
import 'package:hotail/utils/Strings.dart';
import 'package:hotail/utils/Utils.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: primaryColor,
        child: Center(
          child: Column(
            children: [
              Spacer(),
              InkWell(
                onTap: (){
                  navigateRoute(context, "/dashboard");
                },
                child: Image.asset("assets/images/icon.png", width: 90,)
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  APP_NAME,
                  style: TextStyle(
                    fontSize: 34,
                    color: Colors.white,
                    fontWeight: FontWeight.w900
                  ),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
