
import 'package:flutter/material.dart';
import 'package:hotail/utils/Consts.dart';

//Өнгөнүүд
const Color primaryColor = const Color(0xff2ADACF);
const Color accentColor = const Color(0xff03AA00);
const Color successColor = const Color(0xff00FF6C);
const Color errorColor = const Color(0xffEB5757);
const Color inactiveColor = const Color(0xffE8E7EF);
const Color mainBackground = const Color(0xffffffff);
const Color commonTextColor = const Color(0xff767676);
const Color borderColor = const Color(0xffF0F1F7);

//Хэмжээсүүд
const double dialogRadius = 16.0;
const double bigRadius = 30.0;
const double dashboardAppbarRadius = 32.0;
const double baseHorizontalPadding = 16.0;
const double baseElevation = 4.0;
const double cardRadius = 8.0;
const double buttonRadius = 16.0;
const double buttonHeight = 48.0;
const double textFieldHeight = 40.0;

final baseTheme = ThemeData(
  primaryColor: primaryColor,
  accentColor: primaryColor,
  scaffoldBackgroundColor: mainBackground,
  tabBarTheme: TabBarTheme(labelColor: Colors.white, unselectedLabelColor: Colors.white.withAlpha(85)),
  dialogTheme: DialogTheme(
    backgroundColor: mainBackground,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(dialogRadius)),
  ),
  cardTheme: CardTheme(
    elevation: baseElevation,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(cardRadius)),
  ),
  appBarTheme: AppBarTheme(
    color: primaryColor,
    brightness: Brightness.dark,
    iconTheme: IconThemeData(color: Colors.white),
    actionsIconTheme: IconThemeData(color: Colors.white),
    elevation: 0,
    centerTitle: true
  ),
  buttonTheme: ButtonThemeData(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(buttonRadius)),
    ),
    padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
    buttonColor: primaryColor,
  ),
  bottomSheetTheme: BottomSheetThemeData(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(dialogRadius))),
  cardColor: Colors.white,
);

final appbarTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w700,
);