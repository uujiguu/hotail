
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Future<dynamic> navigateRoute(BuildContext context, String path, {bool remove = false, Object arguments, bool popUntil = false}) {
  if (popUntil) {
    Navigator.popUntil(context, ModalRoute.withName(path));
    return null;
  } else {
    if (remove)
      return Navigator.of(context).pushNamedAndRemoveUntil(path, (Route<dynamic> route) => false, arguments: arguments);
    else
      return Navigator.of(context).pushNamed(path, arguments: arguments);
  }
}

String formatValueWithComma(int amount, {String endFix = "₮"}) {
  if (amount != null) {
    final formatter = new NumberFormat("#,###");
    return formatter.format(amount).toString() + endFix;
  } else
    return "0$endFix";
}

String formatValueWithCommaDouble(double amount, {String endFix = "₮"}) {
  if (amount != null) {
    final formatter = new NumberFormat("#,###.##");
    return formatter.format(amount).toString() + endFix;
  } else
    return "0$endFix";
}